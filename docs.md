---
layout: docs
title: Documentation
permalink: /docs/
order: 25
redirect_from: /docs/.
---

Getting reproducible builds for your software or distribution might be easier
than you think.
{: .lead}

However, it might require small changes to your build system and a strategy on
how to enable others to recreate an environment in which the builds can be
reproduced.
